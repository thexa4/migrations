Makes the people in your OpenTTD world migrate to places that provide work.
Transporting cargo from an industry creates jobs that cause unemployed population to move nearby the industry.

# Recommended game settings
 - Towns: Custom(1-20).
 - Industry: between Minimal and Low.
 
## Optional
 - NewGRF Sailing Ships enabled
 - Starting time 1830