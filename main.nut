require("economy_manager.nut")

class MigrationScript extends GSController {
	workQueue = [];
	economyManager = false;

	constructor() {
		local _gridSize = MigrationScript.GetSetting("grid_size");
		local _minPopularity = MigrationScript.GetSetting("city_creation_floor");
		local _populationMultiplier = MigrationScript.GetSetting("population_modifier");
		local _radialTownShrinking = MigrationScript.GetSetting("radial_town_shrinking");
		local _growthStrategy = MigrationScript.GetSetting("growth_strategy");
		economyManager = EconomyManager(this.QueueAction.bindenv(this), _gridSize, _minPopularity, _populationMultiplier, _radialTownShrinking, _growthStrategy);
	}

	function Start();
	function FindSomethingToDo();
	function Save() {
		return {
			unused = 0,
		}
	}
	function Load(version, tbl) {}

	function QueueAction(func);
}

function MigrationScript::QueueAction(func) {
	workQueue.push(func)
}

function MigrationScript::Start() {
	while(true) {

		if (workQueue.len() == 0)
			FindSomethingToDo();

		local currentList = workQueue;
		workQueue = []

		foreach(workItem in currentList) {
			local output = resume workItem;
			if (workItem.getstatus() != "dead")
				workQueue.push(workItem)
			if (output != null && MigrationScript.GetSetting("log_level") == 3)
				GSLog.Info(this.GetTick() + "(" + this.GetOpsTillSuspend() + "): " + output)
		}
		
		if (workQueue.len() == 0)
			this.Sleep(100 - (this.GetTick() % 100));
	}
}

function MigrationScript::FindSomethingToDo() {
	if (!GSGame.IsPaused())
		QueueAction(economyManager.UpdateEconomy());
}
