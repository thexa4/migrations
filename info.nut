class MigrationScriptInfo extends GSInfo {
	function GetAuthor() 		{ return "Max Maton"; }
	function GetName() 		{ return "Migrations"; }
	function GetShortName() 	{ return "MIGR"; }
	function GetDescription() 	{ return "Town migrations"; }
	function GetVersion() 		{ return 4; }
	function MinVersionToLoad() 	{ return 1; }
	function GetDate() 		{ return "2018-09-13"; }
	function UseAsRandomAI() 	{ return true; }
	function CreateInstance() 	{ return "MigrationScript"; }
	function GetAPIVersion() 	{ return "1.4"; }
	function GetURL() 		{ return "https://gitlab.com/thexa4/migrations/"; }

	function GetSettings() {
                AddSetting({name = "log_level", description = "Debug: Log level (higher = print more)", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 3, flags = CONFIG_INGAME, min_value = 1, max_value = 3});
                AddLabels("log_level", {_1 = "1: Info", _2 = "2: Verbose", _3 = "3: Debug" } );
                AddSetting({name = "city_creation_floor", description = "How hard it is for a new town to start.", easy_value = 3000, medium_value = 5000, hard_value = 7000, custom_value = 3000, flags = 0, min_value = 100, max_value = 10000});
                AddSetting({name = "population_modifier", description = "How quickly a town grows once created.", easy_value = 10, medium_value = 8, hard_value = 5, custom_value = 10, flags = 0, min_value = 1, max_value = 100});
                AddSetting({name = "grid_size", description = "Size of industry regions. (higher is faster but results in fewer towns)", easy_value = 48, medium_value = 48, hard_value = 48, custom_value = 48, flags = 0, min_value = 16, max_value = 256});
                AddSetting({name = "radial_town_shrinking", description = "Reduce towns from edges out?", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0, min_value = 0, max_value = 1});
				AddLabels("radial_town_shrinking", {_0 = "0: Off (produces ghost towns)", _1 = "1: On (produces round, smaller towns)"});
				
                AddSetting({name = "growth_strategy", description = "How should cities grow?", easy_value = 1, medium_value = 1, hard_value = 1, custom_value = 1, flags = CONFIG_NONE, min_value = 1, max_value = 2});
                AddLabels("growth_strategy", {_1 = "1: Jobs only", _2 = "2: Jobs+Vanilla" } );
        }
}

RegisterGS(MigrationScriptInfo());
