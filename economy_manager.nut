import("pathfinder.road", "RoadPathFinder", 4);

class EconomyManager {
	constructor(queueFunc, _gridSize, _minPopularity, _populationMultiplier, _radialTownShrinking, _growthStrategy) {
		enqueueAction = queueFunc;
		gridSize = _gridSize;
		minPopularity = _minPopularity;
		populationMultiplier = _populationMultiplier;
		radialTownShrinking = _radialTownShrinking;
		growthStrategy = _growthStrategy;
	}
	
	enqueueAction = false;
	isInitialized = false;
	worldPop = 0;
	gridProductivity = {};
	gridPopularity = {};
	gridTowns = {};
	gridIndustries = {};
	townLowerBounds = {};
	gridDelta = {};
	gridSize = 32;
	townMinima = {};
	townFailures = {};
	minPopularity = 3000;
	populationMultiplier = 10;
	reservePopulation = 0;
	radialTownShrinking = false;
	growthStrategy = 1;
	lastUpdateMonth = -1;
	debugJobs = 0;
	debugJobSeekers = 0;

	function Initialize() {
		if (isInitialized)
			return;
		isInitialized = true;
		worldPop = 0;

		local townPopulations = GSTownList();
		townPopulations.Valuate(GSTown.GetPopulation);
		foreach(townId, townPopulation in townPopulations) {
			if (growthStrategy == 1) {
				GSTown.SetGrowthRate(townId, GSTown.TOWN_GROWTH_NONE);
			}
			worldPop += townPopulation;
		}

		yield "World population: " + worldPop;

		UpdateTowns();
		UpdateIndustry();

		GSLog.Info(gridTowns.len());
		yield "len: " + gridTowns.len();
		foreach(grid, _ in gridTowns) {
			yield "grid: " + grid;
			local towns = gridTowns[grid];
			local townLocations = [];

			if (towns.len() >= 1) {
				local prev = GSTown.GetLocation(towns[0]);
				townLocations = [prev];

				local foundPopulation = 0;
				foreach(i, townId in towns) {
					if (!gridPopularity.rawin(grid))
						gridPopularity[grid] <- 0;
					foundPopulation += GSTown.GetPopulation(townId);
				
					if (i == 0)
						continue; // Source location

					local next = GSTown.GetLocation(townId);
					townLocations.append(next);

					yield "Building intra town road";
					foreach(output in _buildCountryRoad([prev], [next])) { yield output; }
				}
				yield "Found " + foundPopulation + " grid inhabitants";
				local correctedPop = foundPopulation / populationMultiplier;
				gridPopularity[grid] = (correctedPop * correctedPop) + minPopularity;
				yield "Initializing popularity to: " + gridPopularity[grid];
			}

			if (!gridIndustries.rawin(grid) || townLocations.len() == 0)
				continue;

			foreach(i, industryId in gridIndustries[grid]) {
				local destPoints = _getIndustryEntrypoints(GSIndustry.GetLocation(industryId));
				if (destPoints.len() == 0) {
					if (GSController.GetSetting("log_level") >= 2)
						GSLog.Warning("Unable to find industry entry points.");
					break;
				}

				yield "Building industry road";
				foreach(output in _buildCountryRoad(townLocations, destPoints)) { yield output; }
			}
		}
		yield "done"
	}

	function UpdateEconomy() {
		foreach(output in Initialize()) { yield output; }
		
		local currentMonth = GSDate.GetYear(GSDate.GetCurrentDate()) * 12 + GSDate.GetMonth(GSDate.GetCurrentDate());
		if (currentMonth <= lastUpdateMonth)
			return;
		lastUpdateMonth = currentMonth;

		gridProductivity = {}; // Shorter term
		gridTowns = {};
		gridIndustries = {};
		gridDelta = {};

		UpdateTowns();
		UpdateIndustry();

		UpdateGrids();
		//DrawPopularity();

		foreach(output in SimulateGrid()) { yield output; }
		
		yield "Computed month: " + currentMonth + "; " + debugJobSeekers.tointeger() + " job seekers; " + debugJobs.tointeger() + " jobs available";
	}

	function DrawPopularity() {
		local signs = GSSignList();

		foreach(id, _ in signs) {
			GSSign.RemoveSign(id);
		}

		local offset = GSMap.GetTileIndex(gridSize / 2, gridSize / 2);
		local totalPopularity = 0;

		foreach(gridId, value in gridPopularity) {			
			if (value > 0)
				GSSign.BuildSign(gridId + offset, "Popularity: " + value + " => " + gridDelta[gridId]);
			totalPopularity += value;
		}
	}

	function UpdateIndustry() {
		local industries = GSIndustryList();
		foreach(industryId, _ in industries) {
			local grid = _toGrid(GSIndustry.GetLocation(industryId));

			local productivity = 0;
			local cargoes = GSCargoList();
			foreach(cargoId, _ in cargoes) {
				productivity += GSIndustry.GetLastMonthProduction(industryId, cargoId) * GSIndustry.GetLastMonthTransportedPercentage(industryId, cargoId);
			}

			if (!gridIndustries.rawin(grid))
				gridIndustries[grid] <- [];
			gridIndustries[grid].append(industryId);

			if (productivity == 0)
				continue;

			if (!gridProductivity.rawin(grid))
				gridProductivity[grid] <- 0;
			
			gridProductivity[grid] += productivity;

		}
	}

	function UpdateTowns() {
		local towns = GSTownList();
		foreach(townId, _ in towns) {
			local townLocation = GSTown.GetLocation(townId);
			local grid = _toGrid(townLocation);

			local productivity = 0;
			local cargoes = GSCargoList();
			foreach(cargoId, _ in cargoes) {
				productivity += GSTown.GetLastMonthProduction(townId, cargoId) * GSTown.GetLastMonthTransportedPercentage(townId, cargoId);
			}
			
			if (!gridTowns.rawin(grid))
				gridTowns[grid] <- [];
			gridTowns[grid].append(townId);

			if (!gridProductivity.rawin(grid))
				gridProductivity[grid] <- 0;
			
			gridProductivity[grid] += productivity;
		}
	}

	function UpdateGrids() {
		debugJobSeekers = 0;
		debugJobs = 0;
		foreach(gridId, popularity in gridProductivity) {
			if (!gridPopularity.rawin(gridId))
				gridPopularity[gridId] <- 0;
			gridPopularity[gridId] <- (gridPopularity[gridId] * 47 + gridProductivity[gridId]) / 48; // Slowly diffuse
		}
		foreach(gridId, popularity in gridPopularity) {
			local popCapacity = sqrt(popularity > minPopularity ? popularity - minPopularity : 0) * populationMultiplier;

			local totalPopulation = 0;
			local lowerBound = 0;
			local hasTown = false;
			if (gridTowns.rawin(gridId)) {
				foreach(_, townId in gridTowns[gridId]) {
					hasTown = true;
					totalPopulation += GSTown.GetPopulation(townId);
					local townMinPopulation = 50;
					if (townLowerBounds.rawin(townId))
						townMinPopulation = townLowerBounds[townId];
					lowerBound += townMinPopulation;
				}
			}
			
			gridDelta[gridId] <- popCapacity - totalPopulation;
			if (popCapacity > totalPopulation) {
				if (hasTown) {
					debugJobs += gridDelta[gridId];
				}
			} else {
				debugJobSeekers += totalPopulation - (popCapacity > lowerBound ? popCapacity : totalPopulation);
			}
		}
	}

	function SimulateGrid() {
		foreach(gridId, delta in gridDelta) {
			// Has city, delta > 100
			if (gridTowns.rawin(gridId) && delta > 20) {
				// Grow one of the cities in this grid
				local towns = gridTowns[gridId];
				yield "Expanding " + gridId;
				local chosenTown = towns[GSBase.RandRange(towns.len())];
				GSTown.ExpandTown(chosenTown, 1)
				foreach(output in _reducePopulation(10)) { yield output; }
				GSLog.Info("Moving 10 people to " + GSTown.GetName(chosenTown));
				continue;
			}

			// Has no city, delta > 1000
			if (!gridTowns.rawin(gridId) && delta > 300) {
				local success = false;
				local pos = -1;
				yield "Making town in " + gridId + ", " + gridTowns.len();
				// make one, try a few positions
				for (local i = 0; i < 20; i++) {
					local offset = GSMap.GetTileIndex(GSBase.RandRange(gridSize), GSBase.RandRange(gridSize));
					pos = gridId + offset;
					success = GSTown.FoundTown(pos, GSTown.TOWN_SIZE_SMALL, false, GSTown.ROAD_LAYOUT_BETTER_ROADS, null)
					GSLog.Info("Creating new town with a migration of 150");
					if (success)
						break;
				}
				if (!success) {
					yield "failed founding town";
					continue;
				}
				
				foreach(output in _reducePopulation(150)) { yield output; }

				if (success && gridIndustries.rawin(gridId)) {
					foreach(_, industryId in gridIndustries[gridId]) {
						local destPoints = _getIndustryEntrypoints(GSIndustry.GetLocation(industryId));
						if (destPoints.len() == 0) {
							GSLog.Warning("Unable to find industry entry points.");
							break;
						}

						foreach(output in _buildCountryRoad([pos], destPoints)) { yield output; }
					}
				}

				continue;
			}
		}
	}

	function _reducePopulation(amount) {
		local reserveUsed = min(reservePopulation, amount);
		amount -= reserveUsed;
		reservePopulation -= reserveUsed;

		while (amount > 0) {
			amount -= _removeBuilding();
			yield "Population removal, " + amount + " left"
		}
		reservePopulation -= amount;
	}

	function _removeBuilding() {
		local totalPop = 0;
		local currentChoice = null;
		local chosenGrid = null;
		foreach(gridId, towns in gridTowns) {
			if (!gridDelta.rawin(gridId) || gridDelta[gridId] >= 0)
				continue;

			foreach(_, townId in towns) {
				local population = GSTown.GetPopulation(townId);
				if (population <= 50)
					continue; // Can't remove last few buildings
				if (townLowerBounds.rawin(townId)) {
					// If we were unable to destroy any buildings and population didn't change, skip
					if (townLowerBounds[townId] == population)
						continue;
				}
				local currentPop = GSTown.GetPopulation(townId);
				totalPop += currentPop;
				if (GSBase.Chance(currentPop, totalPop)) {
					currentChoice = townId;
					chosenGrid = gridId;
				}
			}
		}

		if (currentChoice == null) {
			GSLog.Info("People are starting to immigrate.");
			return 100; // No valid towns found, assume people are immigrating :)
		}

		local prevPop = GSTown.GetPopulation(currentChoice);

		local tiles = GSTileList();
		local townPos = GSTown.GetLocation(currentChoice);
		local maxRadius = 100;
		local townX = GSMap.GetTileX(townPos);
		local townY = GSMap.GetTileY(townPos);
		local fromX = townX - maxRadius;
		local toX = townX + maxRadius;
		local fromY = townY - maxRadius;
		local toY = townY + maxRadius;
		
		if (fromX < 1)
			fromX = 1;
		if (fromY < 1)
			fromY = 1;
		if (toX >= GSMap.GetMapSizeX() - 1)
			toX = GSMap.GetMapSizeX() - 2;
		if (toY >= GSMap.GetMapSizeY() - 1)
			toY = GSMap.GetMapSizeY() - 2;
		
		local from = GSMap.GetTileIndex(fromX, fromY);
		local to = GSMap.GetTileIndex(toX, toY);
		tiles.AddRectangle(from, to);

		tiles.Valuate(GSTile.GetTownAuthority)
		tiles.KeepValue(currentChoice)
		tiles.Valuate(GSTile.IsBuildable);
		tiles.KeepValue(0); // houses are not buildable
		tiles.Valuate(GSTile.GetOwner);
		tiles.KeepValue(-1);
		tiles.Valuate(GSRoad.IsRoadTile);
		tiles.KeepValue(0);
		tiles.Valuate(_isIndustry);
		tiles.KeepValue(0);
		tiles.Valuate(GSTile.IsWaterTile);
		tiles.KeepValue(0);
		
		if (tiles.Count() == 0) {
			townLowerBounds[currentChoice] <- prevPop;
			GSLog.Info("Unable to locate missing population in " + GSTown.GetName(currentChoice));
			return 1;
		}
		
		if (radialTownShrinking) {
			local valuator = function (t, townPos) {
				return GSBase.RandRange(100) * GSTile.GetDistanceSquareToTile(t, townPos);
			};
			tiles.Valuate(valuator, townPos);
		} else {
			tiles.Valuate(GSBase.RandRangeItem, 100);
		}
		tiles.Sort(GSList.SORT_BY_VALUE, GSList.SORT_DESCENDING);

		foreach(tileId, _ in tiles) {
			local result = GSTile.DemolishTile(tileId);
			local delta = prevPop - GSTown.GetPopulation(currentChoice);
			if (delta > 0) {
				gridDelta[chosenGrid] += delta;
				return delta;
			}
		}

		townLowerBounds[currentChoice] <- prevPop;
		GSLog.Info("Failed to demolish any building in town " + GSTown.GetName(currentChoice));

		return 1; // Retry but end eventually
	}

	function _isIndustry(tileId) {
		return GSIndustry.IsValidIndustry(GSIndustry.GetIndustryID(tileId));
	}

	function _toGrid(tileId) {
		local x = GSMap.GetTileX(tileId);
		local y = GSMap.GetTileY(tileId);
		x = (x / gridSize) * gridSize;
		y = (y / gridSize) * gridSize;
		return GSMap.GetTileIndex(x, y);
	}

	function _buildCountryRoad(from, to) {
		local pathfinder = RoadPathFinder();
		GSRoad.SetCurrentRoadType(GSRoad.ROADTYPE_ROAD);
		pathfinder.cost.turn = 0;
		pathfinder.cost.max_cost = 160 * gridSize;

		pathfinder.InitializePath(from, to);

		local path = false;
		local step = 0;
		while (path == false) {
			path = pathfinder.FindPath(100);
			step += 100;
			yield "Pathfinding step " + step;
		}
		yield "Path found, building";

		if (path == null) {
			if (GSController.GetSetting("log_level") >= 2)
				GSLog.Info("Path failed from " + from + ", to " + to + ".");
		}

		while (path != null) {
			local par = path.GetParent();
			if (par == null)
				break;

			local lastNode = path.GetTile();
			if (GSMap.DistanceManhattan(path.GetTile(), par.GetTile()) == 1) {
				GSRoad.BuildRoad(path.GetTile(), par.GetTile());
				path = par;
				continue;
			}

			// Bridge or tunnel
			if (GSBridge.IsBridgeTile(path.GetTile()) || GSTunnel.IsTunnelTile(path.GetTile())) {
				path = par;
				continue; // Already built, skip
			}

			if (GSRoad.IsRoadTile(path.GetTile()))
				GSTile.DemolishTile(path.GetTile()); // clean up road we're supposed to replace

			if (GSTunnel.GetOtherTunnelEnd(path.GetTile()) == par.GetTile()) {
				GSTunnel.BuildTunnel(GSVehicle.VT_ROAD, path.GetTile());
				path = par;
				continue;
			}

			local bridge_list = GSBridgeList_Length(GSMap.DistanceManhattan(path.GetTile(), par.GetTile()) + 1);
			bridge_list.Valuate(GSBridge.GetMaxSpeed);
			bridge_list.Sort(GSList.SORT_BY_VALUE, true); // Pick slowest (and hopefully cheapest) bridge.

			GSBridge.BuildBridge(GSVehicle.VT_ROAD, bridge_list.Begin(), path.GetTile(), par.GetTile());
			path = par;
		}
	}

	function _getIndustryEntrypoints(location) {

		local maxDist = GSMap.DistanceFromEdge(location);
		if (maxDist > 5)
			maxDist = 5;

		local xoff = GSMap.GetTileIndex(1, 0);
		local yoff = GSMap.GetTileIndex(0, 1);

		local results = [];

		for (local i = 1; i <= maxDist; i+=1)
		{
			local probeLocation = location + i * xoff;
			if (GSTile.IsBuildable(probeLocation) || GSRoad.IsRoadTile(probeLocation)) {
				results.append(probeLocation);
				break;
			}
		}

		for (local i = 1; i <= maxDist; i+=1)
		{
			local probeLocation = location - i * xoff;
			if (GSTile.IsBuildable(probeLocation) || GSRoad.IsRoadTile(probeLocation)) {
				results.append(probeLocation);
				break;
			}
		}
		
		for (local i = 1; i <= maxDist; i+=1)
		{
			local probeLocation = location + i * yoff;
			if (GSTile.IsBuildable(probeLocation) || GSRoad.IsRoadTile(probeLocation)) {
				results.append(probeLocation);
				break;
			}
		}
		
		for (local i = 1; i <= maxDist; i+=1)
		{
			local probeLocation = location - i * yoff;
			if (GSTile.IsBuildable(probeLocation) || GSRoad.IsRoadTile(probeLocation)) {
				results.append(probeLocation);
				break;
			}
		}

		return results;
	}
}
